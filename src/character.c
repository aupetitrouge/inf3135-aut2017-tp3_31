#include "constants.h"
#include "character.h"

struct Character *Character_create(SDL_Renderer *renderer) {
	struct Character *character;
	character = (struct Character*)malloc(sizeof(struct Character));
	character->renderer = renderer;
	character->moving = false;
	character->screenPosition.x = MAP_INITIAL_X;
	character->screenPosition.y = MAP_INITIAL_Y;
	character->xVelocity = 0;
	character->yVelocity = 0;
	character->animatedSpritesheet =
		AnimatedSpritesheet_create(CHARACTER_SPRITESHEET, 6, 32, 192,
									CHARACTER_BETWEEN_FRAME, renderer);
	character->animatedSpritesheet->spritesheet->scale = CHARACTER_SCALE;
	return character;
}

void Character_delete(struct Character *character)
{
	if (character != NULL) {
		AnimatedSpritesheet_delete(character->animatedSpritesheet);
		free(character);
	}
}

void Character_render(struct Character *character)
{
	if (character->moving) {
		int now = SDL_GetTicks();
		if (now > character->currentMove.endTime) {
			character->moving = false;
			AnimatedSpritesheet_stop(character->animatedSpritesheet);
			character->screenPosition = character->currentMove.target;
		} else {
			float t = (now - character->currentMove.startTime) /
					   (float)character->currentMove.duration;
			character->screenPosition.x = (1 - t) * character->currentMove.source.x +
										   t	  * character->currentMove.target.x;
			character->screenPosition.y = (1 - t) * character->currentMove.source.y +
										   t	  * character->currentMove.target.y;
		}
	}
	AnimatedSpritesheet_render(character->animatedSpritesheet,
							   character->screenPosition.x,
							   character->screenPosition.y);
}

void Character_input(struct Character *character,
					SDL_Event event)
{
	if (event.type == SDL_KEYDOWN ) {
		switch (event.key.keysym.sym) {
			case SDLK_UP: character->yVelocity = -CHARACTER_HEIGHT * 2; break;
			case SDLK_DOWN: character->yVelocity = CHARACTER_HEIGHT * 2; break;
			case SDLK_LEFT: character->xVelocity = -CHARACTER_WIDTH * 2; break;
			case SDLK_RIGHT: character->xVelocity = CHARACTER_WIDTH * 2; break;
			default: break;
		}
	} else if (event.type == SDL_KEYUP) {
		switch (event.key.keysym.sym) {
			case SDLK_UP: character->yVelocity = 0; break;
			case SDLK_DOWN: character->yVelocity = 0; break;
			case SDLK_LEFT: character->xVelocity = 0; break;
			case SDLK_RIGHT: character->xVelocity = 0; break;
			default: break;
		}
	}
}

void Character_move(struct Character *character, int duration)
{
	if(!character->moving) {
		character->moving = true;
		int now = SDL_GetTicks();
		character->currentMove.startTime = now;
		character->currentMove.duration = duration;
		character->currentMove.endTime = now + duration;
		character->currentMove.source = character->screenPosition;
		character->currentMove.target = character->screenPosition;

		character->currentMove.target.x += character->xVelocity;
		character->currentMove.target.y += character->yVelocity;
		//Select animation
		if (character->xVelocity < 0) {
			character->currentMove.direction = DIRECTION_LEFT;
			if (character->yVelocity < 0) {
				AnimatedSpritesheet_setRow(character->animatedSpritesheet,
											CHARACTER_JUMPING_LEFT_ROW);
			} else {
				AnimatedSpritesheet_setRow(character->animatedSpritesheet,
											CHARACTER_WALKING_LEFT_ROW);
			}
		} else if (character->xVelocity > 0) {
			character->currentMove.direction = DIRECTION_RIGHT;
			if (character->yVelocity < 0) {
				AnimatedSpritesheet_setRow(character->animatedSpritesheet,
											CHARACTER_JUMPING_RIGHT_ROW);
			} else {
				AnimatedSpritesheet_setRow(character->animatedSpritesheet,
											CHARACTER_WALKING_RIGHT_ROW);
			}
		} else {
			if (character->yVelocity < 0) {
				character->currentMove.direction = DIRECTION_UP;
				AnimatedSpritesheet_setRow(character->animatedSpritesheet,
											CHARACTER_JUMPING_UP_ROW);
			} else {
				character->currentMove.direction = DIRECTION_DOWN;
				AnimatedSpritesheet_setRow(character->animatedSpritesheet,
											CHARACTER_STANDING_STILL_ROW);
			}
		}
		AnimatedSpritesheet_run(character->animatedSpritesheet);
	}
}
